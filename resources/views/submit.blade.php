@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Submit a Task</h1>
            <form action="/submit" method="post">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="url">Url</label>
                    <input type="text" class="form-control" id="url" name="url" placeholder="URL">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" placeholder="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="state">Staus</label>
                    <input class="form-control" id="state" name="state" placeholder="status">
                </div>
                <div class="form-group">
                    <label for="image">Image Path</label>
                    <input class="form-control" id="image_path" name="image" placeholder="image path">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection