<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
use Illuminate\Http\Request;

Route::get('/', function () {
    $todos = \App\Todo::all();
    return view('welcome', ['todos' => $todos]);
});

Route::get('/submit',function(){
    return view('submit');
});

Auth::routes(); 

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/submit', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'title' => 'required|max:255',
        'url' => 'required|max:255',
        'description' => 'required|max:255',
    ]);
    if ($validator->fails()) {
        return back()
            ->withInput()
            ->withErrors($validator);
    }
    $todo = new \App\Todo;
    $todo->title = $request->title;
    $todo->url = $request->url;
    $todo->description = $request->description;
    $todo->image_path = $request->image;
    $todo->state= $request->state;
    $todo->save();
    return redirect('/');
});